![logo](doc/logo.png)

Docker images
=============

The following Docker images are provided for use in GitLab CI when building the
Lely libraries:
* build-essential:
  ubuntu or debian with the build-essential package installed, plus a few
  packages for GitLab CI scripting (apt-transport-https, apt-utils, curl, git,
  pkg-config, software-properties-common and unzip).
  * [bionic-amd64](build-essential/bionic-amd64/Dockerfile): Ubuntu 18.04 LTS
    (Bionic Beaver) (amd64)
  * [focal-amd64](build-essential/focal-amd64/Dockerfile): Ubuntu 20.04 LTS
    (Focal Fossa) (amd64)
  * [jessie-amd64](build-essential/jessie-amd64/Dockerfile): Debian jessie
    (amd64)
  * [buster-amd64](build-essential/buster-amd64/Dockerfile): Debian buster
    (amd64)
* debuild:
  build-essential plus debuild and the Common Debian Build System (CDBS).
  * [bionic-amd64](debuild/bionic-amd64/Dockerfile): Ubuntu 18.04 LTS
    (Bionic Beaver) (amd64)
  * [focal-amd64](debuild/focal-amd64/Dockerfile): Ubuntu 20.04 LTS
    (Focal Fossa) (amd64)
  * [jessie-amd64](debuild/jessie-amd64/Dockerfile): Debian jessie (amd64)
  * [buster-amd64](debuild/buster-amd64/Dockerfile): Debian buster (amd64)
* cross-build-essential:
  build-essential with crossbuild-essential and qemu-user-binfmt.
  * [jessie-armhf](cross-build-essential/jessie-armhf/Dockerfile): Debian jessie
    (armhf)
  * [buster-arm64](cross-build-essential/buster-arm64/Dockerfile): Debian
    buster (arm64)
  * [buster-armhf](cross-build-essential/buster-armhf/Dockerfile): Debian
    buster (armhf)
* cross-debuild:
  cross-build-essential plus debuild and the Common Debian Build System (CDBS).
  * [jessie-armhf](cross-debuild/jessie-armhf/Dockerfile): Debian jessie (armhf)
  * [buster-arm64](cross-debuild/buster-arm64/Dockerfile): Debian buster
    (arm64)
  * [buster-armhf](cross-debuild/buster-armhf/Dockerfile): Debian buster
    (armhf)
* [mingw-w64:focal-amd64](mingw-w64/focal-amd64/Dockerfile):
  build-essential:focal-amd64 plus the [Mingw-w64] compiler suite.
* [wine:focal-amd64](wine/focal-amd64/Dockerfile):
  mingw-w64:focal-amd64 plus [Wine].

All images can be downloaded from the [Container Registry] and [Docker Hub].

Licensing
---------

Copyright 2016-2021 [Lely Industries N.V.]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

[Container Registry]: https://gitlab.com/lely_industries/docker/container_registry
[Docker Hub]: https://hub.docker.com/r/lely/
[Lely Industries N.V.]: http://www.lely.com
[Mingw-w64]: https://mingw-w64.org/
[Wine]: https://www.winehq.org/
